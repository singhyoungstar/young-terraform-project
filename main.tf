provider "aws" {
    region = "ap-south-1" 
}
variable "young-vpc-cidr" {
    description = "young vpc cidr"    
  
}

variable "young-subnet-cidr" {
    description = "young subnet cidr"    
  
}
resource "aws_vpc" "young-vpc" {
    cidr_block = var.young-vpc-cidr
  
}

resource "aws_subnet" "young-subnet-1" {
    vpc_id = aws_vpc.young-vpc.id
    cidr_block = var.young-subnet-cidr
    availability_zone = "ap-south-1a"
    tags = {
        Name:"subnet-1-young"
    }  
}
data "aws_vpc" "existing-vpc" {
    default = true
  
}
resource "aws_subnet" "young-subnet-2" {
    vpc_id = data.aws_vpc.existing-vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "ap-south-1a"
}
output "young-vpc-id" {
    value = aws_vpc.young-vpc.id
  
}
output "young-subnet-id" {
    value = aws_subnet.young-subnet-1.id
  
}